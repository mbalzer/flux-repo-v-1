# flux-repo

This is a proof-of-concept for implementing a [GitOps](https://www.weave.works/technologies/gitops) pipeline
using [Flux](https://fluxcd.io), [Kustomize](https://kustomize.io), and [Helm](https://helm.sh).

## Guide

### Prep

Adding `pre` and `pre-2` clusters to `kubectl` configurations.

```bash
# Use a different config file
$ export KUBECONFIG=~/.kube/config_direct
$ gcloud container clusters get-credentials pre-gitlab-gke --region=us-east1 --project=gitlab-pre
$ gcloud container clusters get-credentials pre-gl-gke-2 --region=us-east1 --project=gitlab-pre
```

### Bootstrap

```bash
$ export KUBECONFIG=~/.kube/config_direct
$ export GITLAB_TOKEN=$(cat ~/.gitlab-token)

$ flux check --pre --context=gke_gitlab-pre_us-east1_pre-gitlab-gke
$ flux bootstrap gitlab \
    --context=gke_gitlab-pre_us-east1_pre-gitlab-gke \
    --owner=gitlab-infra-poc/gitops \
    --repository=flux-repo \
    --branch=main \
    --path=clusters/pre \
    # --author-name=<name for Git commits> \
    # --author-email=<email for Git commits> \
    # --gpg-key-ring=<path to GPG key ring for signing commits> \
    # --gpg-key-id=<key id for selecting a particular key> \

$ flux check --pre --context=gke_gitlab-pre_us-east1_pre-gl-gke-2
$ flux bootstrap gitlab \
    --context=gke_gitlab-pre_us-east1_pre-gl-gke-2 \
    --owner=gitlab-infra-poc/gitops \
    --repository=flux-repo \
    --branch=main \
    --path=clusters/pre-2 \
    # --author-name=<name for Git commits> \
    # --author-email=<email for Git commits> \
    # --gpg-key-ring=<path to GPG key ring for signing commits> \
    # --gpg-key-id=<key id for selecting a particular key> \
```

### Watching Flux Status

```bash
$ flux get kustomizations --watch --context=gke_gitlab-pre_us-east1_pre-gitlab-gke
$ flux get kustomizations --watch --context=gke_gitlab-pre_us-east1_pre-gl-gke-2

$ flux get kustomization <name> --context=gke_gitlab-pre_us-east1_pre-gitlab-gke
$ flux trace kustomization <name> --context=gke_gitlab-pre_us-east1_pre-gitlab-gke
$ flux tree kustomization <name> --context=gke_gitlab-pre_us-east1_pre-gitlab-gke

$ flux get kustomization <name> --context=gke_gitlab-pre_us-east1_pre-gl-gke-2
$ flux trace kustomization <name> --context=gke_gitlab-pre_us-east1_pre-gl-gke-2
$ flux tree kustomization <name> --context=gke_gitlab-pre_us-east1_pre-gl-gke-2

$ flux events --context=gke_gitlab-pre_us-east1_pre-gitlab-gke
$ flux events --context=gke_gitlab-pre_us-east1_pre-gl-gke-2

$ flux logs --context=gke_gitlab-pre_us-east1_pre-gitlab-gke
$ flux logs --context=gke_gitlab-pre_us-east1_pre-gl-gke-2
```

## Design Choices

### Repository Structure

#### Monorepo

In a monorepo approach, all Kubernetes manifests files are stored in a single Git repository. The environment-specific configs are all stored in the same branch.

```
├── apps
│   ├── base
│   ├── production 
│   └── staging
├── infrastructure
│   ├── base
│   ├── production 
│   └── staging
└── clusters
    ├── production
    └── staging
```

The separation between `apps` and `infrastructure` makes it possible to impose an order on a cluster reconcilation process. Each cluster is described by referrecing specifc *apps* and *instrastructure* **overlays** in a dedicated directory under `clusters`.

In this setup, small incremental changes are made to the `main` branch via *short-lived* branches and merge requests.

#### Polyrepo - Repo per Environment

In this approach, we will have a separate Git repository for each environment. Goverance for different environments is easier in this approach and access to the *production* environent can be restricted to only a handful of people in the organization. On the other hand, promoting changes from one environment to another requires more work and automation.

#### Polyrepo - Repo per Tenant

In this approach, a platform team owns the Kubernetes clusters and provisions tenants for other teams. The *clusters* and *tenants* are defined in a Git repository with a structure similar to below:

```
├── infrastructure
│   ├── base
│   ├── production 
│   └── staging
├── clusters
│   ├── production
│   └── staging
└── tenants
    ├── dev
    └── qa
```

A Git repository for each tenant/team looks like the following:

```
└── apps
    ├── base
    ├── production 
    └── staging
```

#### Hybrid Approaches

In an organization with a polyrepo setup, it is common to have a repository for each service and store the Kubernetes manifest files in the same repository. These manifest files can serve as the base config in a *monorepo* or *polyrepo* setup.  Inside the Flux repo, we can define `GitRepository` and `Kustomization` resource to referrence and patch the manifest files from other repositories.

Another approach is bundling a service into a Helm chart and publish it to an artifactory. Then, in the Flux repo, we can define `HelmRepository` and `HelmRelease` resources for each target environment.

## Resources

  - [Flux Docs](https://fluxcd.io/flux/)
    - [Helm Repositories](https://fluxcd.io/flux/components/source/helmrepositories/)
    - [Kustomization](https://fluxcd.io/flux/components/kustomize/kustomization/)
  - [Weave GitOps Docs](https://docs.gitops.weave.works/docs/intro/)
    - [TLS and Certificates](https://docs.gitops.weave.works/docs/configuration/tls/)
    - [Login via an OIDC provider](https://docs.gitops.weave.works/docs/configuration/oidc-access/)
  - [Manage Kubernetes Secrets for Flux with Vault](https://www.hashicorp.com/blog/manage-kubernetes-secrets-for-flux-with-hashicorp-vault)
